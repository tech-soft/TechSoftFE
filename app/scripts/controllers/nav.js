'use strict';

/**
 * @ngdoc function
 * @name calendarServiceApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the calendarServiceApp
 */
angular.module('techSoftApp')
        .controller('NavCtrl',
                ['$scope', '$location',
                    function () {
                        /* off-canvas sidebar toggle */
                        $('[data-toggle=offcanvas]').click(function () {
                            $('.row-offcanvas').toggleClass('active');
                            $('.collapse').toggleClass('in').toggleClass('hidden-xs').toggleClass('visible-xs');
                        });
                    }]);